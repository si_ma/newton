//
//  NewtonSDK.h
//  NewtonSDK
//
//  Created by Ma, Si (UK - London) on 12/7/17.
//  Copyright © 2017 Si Ma. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for NewtonSDK.
FOUNDATION_EXPORT double NewtonSDKVersionNumber;

//! Project version string for NewtonSDK.
FOUNDATION_EXPORT const unsigned char NewtonSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <NewtonSDK/PublicHeader.h>


